<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    public function quarter(){

       return $this->belongsTo('App\Quarter');
    }


    public function trainer()
    {
        return $this->belongsToMany('App\Trainer');
    }
}
