<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trainer extends Model
{
    public function course(){
        
        return $this->belongsToMany('App\Course');
    }

    public function remarks(){
        return $this->morphMany('App\Remark','remarkable');
    }
}
