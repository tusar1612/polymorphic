<?php

$factory->define(App\Student::class, function (Faker\Generator $faker) {


    return [
        'name'=>$faker->name,
        'email'=>$faker->email,
        'phone'=>$faker->phoneNumber,
        'address' =>$faker->streetAddress,
        'highest_education'=>$faker->title,
        'image'=>$faker->imageUrl(),
        'bio' => $faker->paragraphs(5,true),
        'facebook' =>'https://www.facebook.com/'.$faker->unique()->firstName,
        'course_id' => $faker->numberBetween(1,5)
    ];
});